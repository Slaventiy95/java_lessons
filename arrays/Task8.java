/**
 * Created by Admin on 18.11.2015.
 */
public class Task8 {
    public static void main(String args[]) {
        System.out.println(calcNumRoutes(3)==1);

        System.out.println(calcNumRoutes(4)==1);

        System.out.println(calcNumRoutes(5)==3);

        System.out.println(calcNumRoutes(1)==0);

        System.out.println(calcNumRoutes(10)==14);

        System.out.println(calcNumRoutes(22)==912);

        System.out.println(calcNumRoutes(36)==134283);

        System.out.println(calcNumRoutes(40)==560287);

        System.out.println(calcNumRoutes(50)==19908673);

        System.out.println(calcNumRoutes(61)==1011015164);


        System.out.println("Finish");
    }

    static int calcNumRoutes(int n) {
        int[] stones =new int[100];
        stones[0]=1;
        for(int i=0;i<n;i++)
        {
            stones[i+2]+=stones[i];
            stones[i+3]+=stones[i];
            stones[i+5]+=stones[i];
        }
        return stones[n];
    }
}
