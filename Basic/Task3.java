
/**
 * Created by Admin on 04.11.2015.
 */
import java.util.*;
public class task3 {
    public static void main (String[] s)
    {
        System.out.println(sumDigits(717) == 15);
        System.out.println(sumDigits(115) == 7);
        System.out.println(sumDigits(918) == 18);
        System.out.println(sumDigits(5) == 5);
        System.out.println(sumDigits(52) == 7);
        System.out.println(sumDigits(0) == 0);
        System.out.println(sumDigits(103) == 4);
        System.out.println(sumDigits(188) == 17);
        System.out.println("Finish");
    }
static int sumDigits(int x)
{
 assert (x<1000);
    String str= Integer.toString(x);
    int a=str.length();
    int c=0;
    for(int i=0;i<a; i++)
    {
       char b=str.charAt(i);
       int g = Character.getNumericValue(b);
        c+=g;
    }

    return c;
}
}
