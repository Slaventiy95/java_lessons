/**
 * Created by Admin on 05.11.2015.
 */
import java.text.SimpleDateFormat;
public class Task6 {
    public static void main(String[] s)
    {
        System.out.println(getTime(20423).equals("05:40:23"));
        System.out.println(getTime( 5320).equals("01:28:40"));
        System.out.println(getTime(23630).equals("06:33:50"));
        System.out.println(getTime(20381).equals("05:39:41"));
        System.out.println(getTime(42718).equals("11:51:58"));
        System.out.println(getTime(30002).equals("08:20:02"));
        System.out.println(getTime( 3010).equals("00:50:10"));
        System.out.println(getTime(20437).equals("05:40:37"));
        System.out.println(getTime(40315).equals("11:11:55"));
        System.out.println(getTime(18392).equals("05:06:32"));
        System.out.println("Finish");
    }
    static String getTime(int secs) {

        return (getHours(secs) > 9 ? getHours(secs) : "0" + getHours(secs))
                + ":" +(getMinutes(secs)>9?getMinutes(secs):"0"+getMinutes(secs)) + ":" +
                (getSeconds(secs)>9?getSeconds(secs):"0"+getSeconds(secs));
    }

    static int getHours(int seconds) {
        return seconds/60/60;
    }
    static int getMinutes(int seconds) {
        return seconds/60%60;
    }
    static int getSeconds(int seconds) {
        return seconds%60;
    }
}
