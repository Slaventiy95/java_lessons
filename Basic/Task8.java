/**
 * Created by Admin on 05.11.2015.
 */
import java.util.*;
public class Task8 {
    public static void main(String[] s) {
        System.out.println(allDigitsUnique(101) == false);
        System.out.println(allDigitsUnique(566) == false);
        System.out.println(allDigitsUnique(646) == false);
        System.out.println(allDigitsUnique(273) == true);
        System.out.println(allDigitsUnique(859) == true);
        System.out.println(allDigitsUnique(264) == true);
        System.out.println(allDigitsUnique(794) == true);
        System.out.println(allDigitsUnique(660) == false);
        System.out.println(allDigitsUnique(288) == false);
        System.out.println(allDigitsUnique(273) == true);
        System.out.println(allDigitsUnique(252) == false);
        System.out.println(allDigitsUnique(633) == false);
        System.out.println(allDigitsUnique(706) == true);
        System.out.println(allDigitsUnique(776) == false);
        System.out.println(allDigitsUnique(772) == false);
        System.out.println(allDigitsUnique(262) == false);
        System.out.println(allDigitsUnique(461) == true);
        System.out.println(allDigitsUnique(962) == true);
        System.out.println(allDigitsUnique(896) == true);
        System.out.println(allDigitsUnique(343) == false);
        System.out.println("Finish");
    }

    static boolean allDigitsUnique(int x) {
        assert (x < 1000);
        assert (x > 100);
        int[] mas = new int[3];
        String str = Integer.toString(x);
        for (int i = 0; i < 3; i++) {
            mas[i] = str.charAt(i);
        }
        if (mas[0] == mas[1] || mas[0] == mas[2] || mas[1] == mas[2])
            return false;
        else
            return true;

    }
    }

