/**
 * Created by Admin on 04.11.2015.
 */

import java.util.*;
public class Task2 {
    public static void main(String[] args)
        {
            System.out.println(reversNumber(717) == 717);
            System.out.println(reversNumber(411) == 114);
            System.out.println(reversNumber(517) == 715);
            System.out.println(reversNumber(240) == 42);
            System.out.println(reversNumber(900) == 9);
            System.out.println(reversNumber(333) == 333);
            System.out.println("Finish");
    }
    static int reversNumber(int x){
        assert(x<1000);
        assert(x>=100);
        String str = Integer.toString(x);
        String str1= new StringBuffer(str).reverse().toString();
        x=Integer.valueOf(str1);
        return x;
    }
}
